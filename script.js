var symbols = [];
var codes = '';

const papaConfig = {
    delimiter: ",",
    skipEmptyLines: true,
    dynamicTyping: true,
    header: true,
    complete: function(results) {
        console.log("Parsing complete:", results);
        symbols = results.data;
        symbols = convertToObject(symbols);
        codes = Object.keys(symbols).join(',');
        useService();
    },
    error: function(results) {
        console.log("Parsing failed:", results);
    },
    transformHeader: function(header) {
        return header.trim();
    },
};

const input = document.getElementById('prices');

input.onchange = function () {
    const file = input.files[0];
    Papa.parse(file, papaConfig);
};

function convertToObject(arr) {
var obj = {};
arr.forEach(function (el) {
    obj[el.Code] = {
        price: el.Price
    }
});
return obj;
}

function useService() {
    const params = new URLSearchParams({
        region: 'US',
        lang: 'en',
        symbols: codes
    });
    fetch("https://apidojo-yahoo-finance-v1.p.rapidapi.com/market/get-quotes?" + params.toString(), {
        "method": "GET",
        "headers": {
            "x-rapidapi-host": "apidojo-yahoo-finance-v1.p.rapidapi.com",
            "x-rapidapi-key": "6e1d1c2026msh7d5e2f69f6d968cp15cb68jsnb017eb1308cc"
        }
    })
        .then(response => {
            response.json().then( function (data) {
                evaluate(symbols, data.quoteResponse.result);
                }
            );
        })
        .catch(err => {
            console.log(err);
        });
}
function evaluate(symbols, yahooResults) {
    var tableData = [];
yahooResults.forEach(function (yahooResult) {
     var currentSymbolName = yahooResult.symbol;

    tableData.push(
        {
            code: currentSymbolName,
            price: symbols[currentSymbolName].price,
            yahooPrice: yahooResult.regularMarketPrice,
            compare: symbols[currentSymbolName].price === yahooResult.regularMarketPrice
        }
    );
});

    new Tabulator("#example-table", {
        data:tableData,
        layout:"fitColumns",
        columns:[
            {title:"code", field:"code"},
            {title:"price", field:"price"},
            {title:"yahooPrice", field:"yahooPrice"},
            {title:"compare", field:"compare"},

        ],
    });

}


